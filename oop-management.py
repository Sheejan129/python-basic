class Employee:
    def __init__(self, fname, lname, salary, address):
        self.fname = fname
        self.lname = lname
        self.salary = salary
        self.address = address

    def check_tax(self):
        if self.salary > 450000:
            total_tax = (self.salary - 450000)
            return 0.15 * total_tax
        else:
            return self.salary * 0.01

    def fullname(self):
        return '{} {}'.format(self.fname, self.lname)


class Manager(Employee):

    def __init__(self, fname, lname, salary, address, department):
        super().__init__(fname, lname, salary, address)
        self.department = department

    def assign_task(self):
        print('Task Assigned')


manager_1 = Manager('Sheejan', 'Tripathi', 550000, 'Bhaisepati', 'Compputer Science')
print('The tax {} have to pay is {}'.format(manager_1.fullname(), manager_1.check_tax()))
manager_1.assign_task()


class Teacher(Employee):

    def __init__(self, fname, lname, full_time, salary, address, subject):
        super().__init__(fname, lname, salary, address)
        self.full_time = full_time
        self.subject = subject

    def take_class(self):
        print('Class Taken')


teacher_1 = Teacher('John', 'Doe', True, 450000, 'Bhaisepati', 'Python')
print('The tax {} have to pay is {}'.format(teacher_1.fullname(), teacher_1.check_tax()))
teacher_1.take_class()


class Helper(Employee):

    def __init__(self, fname, lname, salary, address):
        super().__init__(fname, lname, salary, address)

    def clean_desk(self):
        print('Desk Cleaned')

    def manage_parking(self):
        print('Parking Managed')


helper_1 = Helper('John', 'Wick', 250000, 'Kalanki')
print('The tax {} have to pay is {}'.format(helper_1.fullname(), helper_1.check_tax()))
helper_1.clean_desk()
helper_1.manage_parking()
