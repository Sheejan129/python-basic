import os
from PIL import Image,ImageEnhance

SRC_DIR = r'C:\Users\Sheejan Tripathi\Desktop\images'
DEST_DIR = r'C:\Users\Sheejan Tripathi\Desktop\Brighter Images'


for item in os.listdir(SRC_DIR):
    #print(item)
    full_path = os.path.join(SRC_DIR, item)
    save_full_path = os.path.join(DEST_DIR, item)
    extension = item.split('.')[-1]
    im=Image.open(full_path)
    enh=ImageEnhance.Brightness(im)
    enh.enhance(1.2).save(save_full_path)

    bw = im.convert('L')
    bw.show()