class Employee:
    def __init__(self, first, last, pay):
        self.first = first
        self.last = last
        self.pay = pay
        self.email = first + '.' + last + '@company.com'

    def fullname(self):
        return '{} {}'.format(self.first, self.last)


emp_1 = Employee('Sheejan', 'Tripathi', 50000)
emp_2 = Employee('Wayne', 'Rooney', 300000)
print(emp_1.fullname())
print(emp_2.fullname())
print('{} is paid {}'.format(emp_1.fullname(), emp_1.pay))
print('{} is paid {}'.format(emp_2.fullname(), emp_2.pay))
print("The Email-ID of {} is {}".format(emp_1.fullname(), emp_1.email))
print("The Email-ID of {} is {}".format(emp_2.fullname(), emp_2.email))
