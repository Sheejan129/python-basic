import math
a=[4,10,14,2,13,14,15,19,9,12,85,34]
mean=sum(a)/len(a)
print("Mean is:",mean)
total=0
for x in a:
    sd=(x-mean)**2/len(a)
    total+=sd
print("Standard deviation is:",math.sqrt(total))

b=a.copy()
b=sorted(b)
print("Sorted list is:",b)
for i in range(len(b)):
    if len(b)%2!=0 and i == (len(b)+1)/2:
        print("Median is :",b[i-1])
    else:
        left=(len(b)+1)//2
        right=(len(b)-1)//2
        print("MEdian is: ",(b[left]+b[right])/2)
        break
print()

sample_list=[2,3,4,5]
print("Sample list is:",sample_list)
a=[]
for elem in sample_list:
    a.append(elem**2)
print("Squared element of sample_list is: ",a)
print()

def even_odd(num):
    if num%2==0:
        return "Even  number"
    else:
        return "Odd number"
print(even_odd(5))

check_even_odd=lambda n: n%2 == 0
print(check_even_odd(5))

my_sum=lambda a,b: a+b
print(my_sum(10,2))

rev_string=lambda s: s[::-1]
print(rev_string('sheejan'))

string=lambda strr:strr.title()
print(string('sheejan'))

string1=lambda str1:str1[::-1]==str1
print(string1('liril'))

var=lambda p,b:math.sqrt((p**2)+(b**2))
print(var(3,4))

a=[1,2,3,4,5]
mean=lambda a:sum(a)/len(a)
print(mean(a))

sample_list=['ram','shyam','sheejan','hari']
sample_list1=[]
for elem in sample_list:
    sample_list1.append(elem[::-1])
print(sample_list1)

#new_way
new_way=map(lambda s:s[::-1],sample_list)
print(list(new_way))

sqr_num=[2,4,6,8]
res=map(lambda s:s**2,sqr_num)
print(list(res))

result=map(lambda s:"Mr. "+s,sample_list)
print(list(result))



total_marks=[750, 659, 560, 450, 720, 700]
result1=map(lambda marks: marks/8,total_marks)
#print(list(result1))

data=list(result1)
def grade(marks):
    if marks >= 80:
        return "Distinction"
    elif marks >= 60:
        return "First div"
    elif marks >= 50:
        return "second div"
    elif marks >= 40:
        return "Third div"
    else:
        return "Fail"

result2=map(grade,data)
print(list(result2))

list11=[2,3,4,5]
res1=map(lambda a:a**-1,list11)
print(list(res1))

new_numbers=[2,3,4,5,6,7,8]
filered_num=filter(lambda n:n%2==0,new_numbers)
print(list(filered_num))



