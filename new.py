#*
#**
#***
#****
#*****
res="*"
n=6
for i in range(1,n):
    print(res)
    res+='*'
print('\n')

# for i in range(6):
#     print('*'*i)
# print('\n')

#*****
#****
#***
#**
#*

# for i in range(5,0,-1):
#     print('*'*i)
# print('\n')

for row in range(0,6):
    for col in range(0,6-row):
        print('*',end='')
    print()




#     *
#    **
#   ***
#  ****
# *****


spaces=5
for i in range(0,6):
    for j in range(0,spaces):
        print(end=" ")
    spaces=spaces-1
    for j in range(0,i):
        print('*',end="")
    print('\r')
print('\n')

#*****
# ****
#  ***
#   **
#    *

for i in range(6,0,-1):
    print((6-i)*' '+i*'*')
print('\n')

#       *
#      * *
#     * * *
#    * * * *
#   * * * * *
#  * * * * * *
# * * * * * * *
# * * * * * * *
#  * * * * * *
#   * * * * *
#    * * * *
#     * * *
#      * *
#       *

for i in range(0,7):
    print(' '*(7-i-1)+'* '*(i+1))
for j in range(7,0,-1):
    print(' '*(7-j)+'* '*(j))
print('\n')

max_no_of_stars=15
space_count=max_no_of_stars//2
star_count=1
for i in range(max_no_of_stars):
    if star_count-2>0:
        print(' '*space_count+'*'+" "*(star_count-2)+'*')
    else:
        print(' '*space_count+'*')
    if i<max_no_of_stars//2:
        space_count-=1
        star_count+=2
    else:
        space_count+=1
        star_count-=2
print('\n')
#
#  *
# *
# *
#  *
#   *
#   *
#  *
# *

for row in range(9):
    for col in range(3):
        if (row+col==2) or (row-col==3) or (row+col==8):
            print('*',end='')
        else:
            print(" ",end="")
    print()
print('\n')

#   *
#  * *
# *   *
# *   *
#  * *
#   *

n1=int(input("Enter a number: "))
num1=n1//2
for row1 in range(n1):
    for col1 in range(n1):
        if (row1+col1 == num1) or (row1-col1 == num1) or (col1-row1 == num1) or (row1+col1 == n1+num1-1):
            print('*',end="")
        else:
            print(end=" ")
    print()
print('\n')



#     *
#    ***
#   *****
#  *******
# *********
#     *



tree_height=int(input("How tall is the tree:"))
spaces=tree_height-1
star=1
stump_spaces=tree_height-1

while tree_height != 0:
    for i in range(spaces):
        print(' ',end="")
    for i in range(star):
        print('*',end="")
    print()
    spaces=spaces-1
    star=star+2
    tree_height=tree_height-1

for i in range(stump_spaces):
    print(' ',end="")
print("*")





