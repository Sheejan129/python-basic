sample_list=["AAshish","Sheejan","raju","sagar"]
for i in sample_list:
    print(i)

print(sample_list[:2])
print(sample_list[0:4:2])
print(sample_list[::-1])

sample_list2=["football",1,2]
for elem in sample_list2:
    print(elem)

print(sample_list2[:2:2])
del sample_list2[2]
print(sample_list2)
sample_list2.append('cricket')
print(sample_list2)
sample_list2[1]="Basketball"
print(sample_list2)
print(sample_list2.count('football'))
sample_list2.insert(1,'hockey')
print(sample_list2)
sample_list2.reverse()
print(sample_list2)
print(sample_list2.index('hockey'))
last=sample_list2.pop()
print(last)
sample_list2.append(last)
print(sample_list2)

res=sample_list.join()
print(res)