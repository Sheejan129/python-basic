import math
# x1 = 2
# x2 = 4
# y1 = 4
# y2 = 8
# value = (x1 - x2)**2 + (y1 - y2)**2
# dist = float(math.sqrt(value))
# print("The dist is",dist)

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return 'Point(%f, %f)'%(self.x, self.y)

    def quad(self):
        if (self.x == 0 or self.y == 0):
            return None
        if (self.x>0 and self.y>0):
            return 1
        if (self.x < 0 and self.y > 0):
            return 2
        if (self.x < 0 and self.y < 0):
            return 3
        if (self.x > 0 and self.y < 0):
            return 4

# a = Point()
# a.x = 5
# a.y = 3
a = Point(4, 5)
# b = Point()
# b.x = 7
# b.y = 10
b = Point(7, 10)
# c = Point()
# c.x = -2
# c.y = 5
c = Point(-2, 5)
# d = Point()
# d.x = -4
# d.y = -5
d = Point(-4, -5)
# e = Point()
# e.x = 5
# e.y = -3
e = Point(5, -3)
# f = Point()
# f.x = 0
# f.y = 0
f = Point(0, 0)
distance = math.sqrt((a.x + b.x)**2 + (a.y + b.y)**2)
print(type(b))
print("The diatance is",distance)

print(a)

# print("a is in", a.quad(), 'quadrant')
# print("b is in", b.quad(), 'quadrant')
# print("c is in", c.quad(), 'quadrant')
# print("d is in", d.quad(), 'quadrant')
# print("e is in", e.quad(), 'quadrant')
# print("f is in", f.quad(), 'quadrant')