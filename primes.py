#Program to check if the number is prime
def is_prime(number):
    if number==2:
        return True
    if number%2==0 or number<=1:
        return False
    if number%3==0:
        return False
    return True
number=int(input("Enter a number: "))
print(is_prime(number))

#divisors

num=int(input("Enter a number: "))
div_list=[]
for i in range(1,num+1):
    if num % i == 0:
        div_list.append(i)

print(div_list)

#list overlap

a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
list=[]
for elem in a:
    if elem in b:
        if elem not in list:
            list.append(elem)
print(list)

#palindrome or not

string=input("Enter a string: ")
if string==string[::-1]:
    print("The string is palindrome")
else:
    print("The string is not palindrome")

#list comprehension

a1 = [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
list1=[]
for i in a1:
    list1.append(i)
print(list1)



