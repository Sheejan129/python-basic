def is_pytha(a, b, c):
    return a**2+b**2==c**2

for a in range(1,1001):
    for b in range(1, 1001):
        c=1000-a-b
        if a+b+c==1000 and is_pytha(a,b,c):
                print(a,b,c)
                print(a*b*c)
                exit(0)

num=int(input("Enter the number of rows: "))
for i in range(1,num+1):
    print(' '*(num-i),end="")
    for j in range(i,0,-1):
        print(j,end="")
    for j in range(2,i+1):
        print(j,end="")
    print()


