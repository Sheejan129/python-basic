import csv


def only_below_25(row):
    parts = row['DOB'].split('/')
    age = 2018 - int(parts[-1])
    return age < 25


with open('Book1.csv', 'r', newline='\n') as samp_file:
    # reader = csv.DictReader(samp_file)
    # ages = filter(only_below_25, reader)
    # with open('ages.csv', 'w', newline='\n') as write_file2:
    #     writer2 = csv.DictWriter(write_file2, fieldnames=['Name', 'Gender', 'Marks', 'DOB'])
    #     writer2.writeheader()
    #     writer2.writerows(ages)

    reader1 = csv.DictReader(samp_file)

    converted_to_list = list(reader1)
    males = filter(lambda row: row['Gender'] == 'Male', reader1)
    with open('Males_only.csv', 'w', newline='\n') as write_file1:
        writer1 = csv.DictWriter(write_file1, fieldnames=['Name', 'Gender', 'Marks', 'DOB'])
        writer1.writeheader()
        writer1.writerows(males)
    # females=filter(lambda row: row['Gender'] == 'Female',reader)
    # for i in females:
    #     print(i)

    reader2 = csv.DictReader(samp_file)
    marks = filter(lambda mark: int(mark['Marks']) <= 50, reader2)
    with open('Marks_Less_than_50.csv', 'w') as write_file:
        writer = csv.DictWriter(write_file, fieldnames=['Name', 'Gender', 'Marks', 'DOB'])
        writer.writeheader()
        writer.writerows(marks)

    # reader = csv.DictReader(samp_file)
    # current_year=2018
    # age=map(lambda a: current_year-a['DOB'],reader)
    # print(list(age))
