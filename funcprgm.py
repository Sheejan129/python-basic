sample_names=["Ram bhattarai", "Sheejan tripathi", "raju mandal", "ranjeet tripathi", "sagar risal", "sayuj thakuri"]
def function(elem):
    a=elem.split(' ')
    return a[-1]
result=map(function,sample_names)
print(list(result))

def funcc(elem1):
    b=elem1.split(' ')
    if 'tripathi' in b:
        return b
result1=filter(funcc,sample_names)
print(list(result1))

sample_list = [2, 4, 5, 8, 10, 13, 15, 50, 42]
divisible_by_5 = lambda s:s%5 == 0
multiple_of_5=filter(divisible_by_5, sample_list)
print(list(multiple_of_5))

names=['Ram pd sharma', 'Hari Bhusal', 'Soman bd Yadav', 'Josh Taylor', 'Roshan k. Rijal']

name_without_middle_name=filter(lambda name: len(name.split(' '))==2, names)
print(list(name_without_middle_name))

name_with_middle_name=filter(lambda name: len(name.split(' '))==3, names)
print(list(name_with_middle_name))

import math
sam_list=[1, 3, 5, 4, 25, 36, 64, 34, 65]
perfect_square=filter(lambda num:math.sqrt(num)==int(math.sqrt(num)),sam_list)
print(list(perfect_square))

multiple_of_3=filter(lambda n:n%3==0,sample_list)
print(list(multiple_of_3))

location=['kathmandu,np', 'Lumbini,np', 'goa,in','new york,usa', 'texas,usa','pkr,np']
def loc(elem1):
    a=elem1.split(',')
    if a[-1]=='np':
        return a
res=filter(loc,location)
print(list(res))

res1=filter(lambda n:n.split(',')[-1]=="np",location)
print(list(res1))

names_list=['Muhammad ali khan','sheejan tripathi','raju mandal','uttam thapa magar','ram bahadur kc']
res2=filter(lambda name:len(name)>=15,names_list)
print(list(res2))



#numbers_list=[1,2,4,77,5,54,45,87,645,153,370,371,407]
#res3=filter(lambda num:)

num_lists=[0,-5,10,5,-6,7,8,-2]
res3=filter(lambda n:n>0,num_lists)
print(list(res3))

