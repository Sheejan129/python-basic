class Customer:
    def __init__(self, name, balance, address, account_type):
        self.name = name
        self.__balance = balance
        self.address = address
        self.account_type = account_type

    def deposit(self, amount):
        self.__balance += amount

    def withdraw_amount(self, amount):
        if self.__balance >= amount:
            self.__balance = self.__balance - amount
        else:
            print('Insufficient Balance')

    def print_balance(self):
        print(self.__balance)

customer_1 = Customer('Sheejan Tripathi', 1000000, 'Bhaisepati', 'Mutual')
customer_1.deposit(20000)
customer_1.print_balance()
print('{} lives in {}.'.format(customer_1.name, customer_1.address) )
#print(customer_1.__balance)

#customer_1.__balance = 0
#print(customer_1.__balance)