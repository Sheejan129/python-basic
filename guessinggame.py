import random
number=random.randint(1,10)
count=0
guess=0
while guess!=number and guess!='exit':
    guess = input("Guess a number:")
    if guess.lower()=='exit':
        break
    guess=int(guess)
    count+=1
    if guess>number:
        print("Guess a lower number.")
    elif guess<number:
        print("Guess a higher number.")
    else:
        print("The number was indeed ",guess)
        print("It's taken you {} tries".format(count))
        break

