import math


class Rectangle:
    def __init__(self, l, b):
        self.l = l
        self.b = b

    def area(self):
        return self.l * self.b

    def perimeter(self):
        return 2 * (self.l + self.b)

    def square(self):
        if self.l == self.b:
            return "It's a Square"
        else:
            return "It's not a square"

    def diagonal(self):
        return math.sqrt((self.l ** 2) + (self.b ** 2))


a = Rectangle(5, 7)
# a = Rectangle()
# a.l = 5
# a.b = 7

print("The area is ", a.area())
print("The perimeter is ", a.perimeter())
print(a.square())
print("The length of diagonal is", a.diagonal())


class Triangle:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def area(self):
        s = (self.a + self.b + self.c) / 2
        return math.sqrt(s * (s - self.a) * (s - self.b) * (s - self.c))

    def check(self):
        if (self.a == self.b == self.c):
            return "The Triangle is Equilateral"
        if (self.a == self.b) or (self.b == self.c) or (self.a == self.c):
            return "The Triangle is Isosceles"
        if (self.a != self.b != self.c):
            return "The Triangle is scalene"


sample_triangle = Triangle(5, 6, 7)
# sample_triangle.a = 5
# sample_triangle.b = 6
# sample_triangle.c = 7

print("The area of triangle is", sample_triangle.area())
print(sample_triangle.check())


class Circle:
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return math.pi * self.radius ** 2


sample_circle = Circle(5)
# sample_circle.radius = 5
print("The area of circle is", sample_circle.area())


class Cuboid():
    def __init__(self, l, b, h):
        self.l = l
        self.b = b
        self.h = h

    def volume(self):
        return self.l * self.b * self.h

    def tsa(self):
        return sample_cuboid.volume() + 2 * (self.l + self.b) * self.h


sample_cuboid = Cuboid(5, 6, 7)
# sample_cuboid.l = 5
# sample_cuboid.b = 6
# sample_cuboid.h = 7
print("The volume is", sample_cuboid.volume())
print("The total surface area is", sample_cuboid.tsa())
