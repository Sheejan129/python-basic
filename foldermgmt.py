import os

BASE_DIR= r'C:\Users\Sheejan Tripathi\Desktop\random'

file_types= {
    'photos': ['jpg', 'JPG'],
    'Documents': ['csv', 'pdf','txt'],
    'Movies': ['mkv']
}

#For creating folder
for file_type in file_types:
    full_path=os.path.join(BASE_DIR, file_type)
    if not os.path.exists(full_path):
        os.mkdir(full_path)

#logic for movinf files to folcer
for file in os.listdir(BASE_DIR):
    #print(file)
    ext=file.split('.')[-1]
    for file_type in file_types:
        #print(file_type, file_types[file_type])
       if ext in file_types[file_type]:
            path_src = os.path.join(BASE_DIR,file)
            # print(path_src)
            path_dest=os.path.join(BASE_DIR,file_type, file)
            #print(path_dest)
            os.rename(path_src,path_dest)
