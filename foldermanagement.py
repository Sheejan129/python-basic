import os
from datetime import datetime, timezone

main_dir = r'C:\Users\Sheejan Tripathi\Desktop\photos'
m = []
file_types = {
    'january': ['01'],
    'february': ['02',],
    'march': ['03',],
    'april': ['04',],
    'may': ['05',],
    'june': ['06'],
    'july': ['07'],
    'august': ['08'],
    'september': ['09'],
    'october': ['10'],
    'november': ['11'],
    'december': ['12']

}


# file_types= {
#     'photos': ['jpg'],
#     'Documents': ['csv', 'pdf','txt'],
#     'Movies': ['mkv']
# }

#print(os.listdir(main_dir))
for elem in os.listdir(main_dir):
    file_path = os.path.join(main_dir, elem)
    mod_time = os.path.getmtime(file_path)
    utc_time = datetime.fromtimestamp(mod_time, timezone.utc)
    local_time = utc_time.astimezone()
    modified_time = local_time.strftime("%Y-%m-%d %H:%M:%S.%f%z (%Z)")
    m.append(modified_time)


for file_type in file_types:
    folder_path = os.path.join(main_dir, file_type)
    # print(folder_path)
    if not os.path.exists(folder_path):
        os.mkdir(folder_path)

for item in m:
    extension = item.split('-')[-2]
    print(extension)

# for file in os.listdir(main_dir):
#     for file_type in file_types:
#         #print(file_type, file_types[file_type])
#         if extension in file_types[file_type]:
#             src_path = os.path.join(main_dir, file)
#             print(src_path)
#             dest_path = os.path.join(main_dir, file_type, file)
#             print(dest_path)
#             os.rename(src_path, dest_path)




# from datetime import datetime, timezone
# import os
# a = r'C:\Users\Sheejan Tripathi\Desktop\random'
# utc_time = datetime.fromtimestamp(os.path.getmtime(a), timezone.utc)
# local_time = utc_time.astimezone()
# print(local_time.strftime("%Y-%m-%d %H:%M:%S.%f%z (%Z)"))
