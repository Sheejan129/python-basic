class Employee:

    def __init__(self, fname, lname, salary, m_status):
        self.fname = fname
        self.lname = lname
        self.salary = salary
        self.m_status = m_status

    def fullname(self):
        return '{} {}'.format(self.fname, self.lname)

    def check_tax(self):
        if self.m_status == True and self.salary>450000:
            total_tax = (self.salary - 450000)
            return 0.15 * total_tax
        elif self.m_status == False and self.salary> 350000:
            total_tax = (self.salary - 350000)
            return 0.15 * total_tax
        else:
            return self.salary * 0.01


emp1 = Employee('Sheejan','Tripathi',450000,False)
print("The tax {} have to pay is {}".format(emp1.fullname(),emp1.check_tax()))
